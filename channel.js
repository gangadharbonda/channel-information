function load() {
  //  $(document).ready(function(){
                      
                                        $.ajax({
                                               type: "GET",
                                               url: 'http://data-production.cspire.tv/guide/v5/lineup/cspire/paytv/1.0/default/live/all-channels.json',
                                               //url: 'http://data-staging.cspire.tv/guide/v5/lineup/cspire/paytv/1.0/default/live/all-channels.json',
                                               async:true,
                                               crossDomain:true,
                                               dataType: "json",
                                               success: function(data, status, xhr) {
                                                   //alert(xhr.getResponseHeader('Location'));
                                                   var x = document.getElementById("flavors").value;
                                                   var y = document.getElementById("environment").value;
                                               
                                                   $('#channels').empty();
                                                   $('#channels').append('Number of channels::' + data.total);
                                                   var channelItems = data.inventories;
                                                   $('#channelInformation').empty();
                                                   var result = "<table>" + "<tr>" + "<th>Name</th>" + "<th>Channel Number</th>" + "<th>Catchup Enabled</th>" + "<th>Timeshift Enabled</th>" +"<th>Recording Enabled</th>" +"<th>Startover Enabled</th>" +"<th>Out of home enabled</th>"  + "<th>        Region       </th>" + "<th> DRM Rights -- Play</th>"  + "<th> DRM Rights -- Catchup</th>"  + "<th> DRM Rights -- Recording</th>"  + "</tr>"
                                                   
                                                   $.each(channelItems,function(index,channelItem) {
                                                          var catchupColumn = "<th bgcolor=\"#FF0000\">" + "NO" + "</th>";
                                                          var timeshiftColumn = "<th bgcolor=\"#FF0000\">" + "NO" + "</th>";
                                                          var recordingColumn = "<th bgcolor=\"#FF0000\">" + "NO" + "</th>";
                                                          var startoverColumn = "<th bgcolor=\"#FF0000\">" + "NO" + "</th>";
                                                          var outOfHomeColumn = "<th bgcolor=\"#FF0000\">" + "NO" + "</th>";
                                                          var regionsColumn = "";
                                                          var drmRightsPlayColumn = "";
                                                          var drmRightsCatchupColumn = "";
                                                          var drmRightsRecordingColumn = "";
                                                          //Catchup enabled
                                                          if (channelItem.is_catchup_enabled) {
                                                            catchupColumn = "<th bgcolor=\"#BBFF00\">" + channelItem.catchup_duration/3600 + " hrs"+ "</th>";
                                                          }
                                                          //Timeshift enabled
                                                          if (channelItem.is_timeshift_enabled) {
                                                            let timeShiftDuration = channelItem.timeshift_duration /3600
                                                            timeshiftColumn = "<th bgcolor=\"#BBFF00\">" + timeShiftDuration + ((timeShiftDuration>1)? " hrs":" hr" )+ "</th>";
                                                          }
                                                          //Recording enabled
                                                          if (channelItem.is_recording_enabled) {
                                                            recordingColumn = "<th bgcolor=\"#BBFF00\">" + "YES" + "</th>";
                                                          }
                                                          //Start over enabled
                                                          if (channelItem.is_startover_enabled) {
                                                            startoverColumn = "<th bgcolor=\"#BBFF00\">" + "YES" + "</th>";
                                                          }
                                                          //Out of home enabled
                                                          if (channelItem.is_out_of_home_playback_enabled) {
                                                          outOfHomeColumn = "<th bgcolor=\"#BBFF00\">" + "YES" + "</th>";
                                                          }
                                                          //Multiple regions
                                                          if (channelItem.hasOwnProperty('regions')) {
                                                             regionsColumn = "<th bgcolor=\"#BBFF00\"> National"
                                                              var regions = channelItem.regions
                                                              for (i =0;i<regions.length;i++) {
                                                                      regionsColumn+= "<br>" + regions[i]
                                                                  }
                                                            regionsColumn+= "</th>";
                                                          }else {
                                                            regionsColumn = "<th bgcolor=\"#BBFF00\">" + "National" + "</th>";
                                                          }
                                                          //DRM rights
                                                          var drmRights = channelItem.drm_rights;
                                                          if (drmRights.length > 0) {
                                                            console.log(drmRights);
                                                            console.log
                                                             var playList = ""
                                                             var catchList = ""
                                                             var recordingList = ""
                                                          
                                                              for (x in drmRights) {
                                                                  if (drmRights[x].includes("recording::")) {
                                                                   recordingList += drmRights[x].replace("recording::","") + "<br>"
                                                                  }else if (drmRights[x].includes("play::")) {
                                                                    playList += drmRights[x].replace("play::","") + "<br>";
                                                                  }else if (drmRights[x].includes("catchup::")) {
                                                                    catchList += drmRights[x].replace("catchup::","") + "<br>";
                                                                  }
                                                              }
                                                              if (playList.length > 0) {
                                                                 drmRightsPlayColumn = "<th>" + playList + "</th>";
                                                              }else {
                                                                 drmRightsPlayColumn = "<th bgcolor=\"#AAAF0A\">" + "N/A" + "</th>";
                                                              }
                                                              if (catchList.length > 0) {
                                                                console.log(catchList);
                                                                 drmRightsCatchupColumn = "<th>" + catchList + "</th>";
                                                              }
                                                              else {
                                                                drmRightsCatchupColumn = "<th bgcolor=\"#AAAF0A\">" + "N/A" + "</th>";
                                                              }
                                                              if (recordingList.length > 0) {
                                                                 drmRightsRecordingColumn = "<th>" + recordingList + "</th>";
                                                              }
                                                             else {
                                                                drmRightsRecordingColumn = "<th bgcolor=\"#AAAF0A\">" + "N/A" + "</th>";
                                                              }
                                                          }
                                                          
                                                          
                                                          result+= "<tr>" + "<th>" + channelItem.name + "</th>" + "<th>" + channelItem.channel_number + "</th>"+ catchupColumn + timeshiftColumn + recordingColumn + startoverColumn + outOfHomeColumn + regionsColumn + drmRightsPlayColumn + drmRightsCatchupColumn + drmRightsRecordingColumn + "</tr>";
                                                          })
                                                   
                                                   result = result + "</table>";
                                                   $("#channelInformation").append(result);
                                               }
                                               });
                                        
                      
      //                });
    }


